function processData(input) {
    Controller.parseInput(input);

}
class Matrix3d {
    //Creates a new x*y*z matrix.
    constructor(x,y,z){
        this.m = new Array(x);
        for(var i=0;i<x;i++){
            this.m[i] = new Array(y);
            for(var j=0;j<y;j++){
                this.m[i][j] =  new Array(z).fill(0);
            }
        }
    }
}

class Controller {
    static parseInput (input){
        var lines = input.split("\n");
        var testCases = lines.splice( 0, 1 );

        for(var i = 0;i<testCases;i++) {

            var pairInfo = lines[0].split(" ");

            var matrixSize = parseInt(pairInfo[0]);
            var numberOfOperations = parseInt(pairInfo[1]);


            var matrix = new Matrix3d(matrixSize, matrixSize, matrixSize);

            lines.splice(0, 1);
            for (var j = 0; j < numberOfOperations; j++) {
                var data = lines[j].split(" ");
                var action = data.splice(0, 1).toString();

                data = data.map(function (n) {
                    return parseFloat(n) - 1;
                });

                switch (action) {
                    case "UPDATE":
                        var [x, y, z, w] = data;
                        Update.updateBlock(x, y, z, w + 1, matrix);
                        break;
                    case "QUERY":
                        var [x1, y1, z1, x2, y2, z2] = data;
                        Query.sumBlocksQuery(x1, y1, z1, x2, y2, z2, matrix);
                        break;
                    default:
                        throw "Unexpected action: " + action;
                }
                ;
            }
            lines.splice(0, numberOfOperations);


        }

    }
}

class Query{
    static sumBlocksQuery(x1,y1,z1,x2,y2,z2,matrix){
        if(x1===x2&&y1===y2&&z1===z2){
            var result = matrix.m[x1][y1][z1];
            console.log(result);
        }else {
            var newSizeX = x2 - x1 + 1;
            var newSizeY = y2 - y1 + 1;
            var newSizeZ = z2 - z1 + 1;
            var result = 0;
            for (var i = 0; i < newSizeX; i++) {
                for (var j = 0; j < newSizeY; j++) {
                    for (var k = 0; k < newSizeZ; k++) {
                        result += matrix.m[x1 + i][y1 + j][z1 + k];
                        //subMatrix.update(i,j,k,this.matrix[x1+i][y1+j][z1+k]);
                    }
                }
            }
            console.log(result);
        }
    }
}

class Update{
    static updateBlock(x,y,z,w,matrix){
        matrix.m[x][y][z]=w;
    };
}

processData("2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2");