/**
 * Created by Juan Daniel Morales on 10/19/2016.
 */
class Matrix3d {
    //Creates a new x*y*z matrix.
    constructor(x,y,z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.m = new Array(x);
        for(var i=0;i<x;i++){
            this.m[i] = new Array(y);
            for(var j=0;j<y;j++){
                this.m[i][j] =  new Array(z).fill(0);
            }
        }

    }
}