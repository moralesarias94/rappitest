/**
 * Created by Juan Daniel Morales on 10/19/2016.
 */
function processData(input) {
    Controller.parseInput(input);
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
    processData(_input);
});