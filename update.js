/**
 * Created by Juan Daniel Morales on 10/22/2016.
 */
//Updates the value of a specific block in the 3dimensional matrix.
class Update{
    static updateBlock(x,y,z,w,matrix){
        matrix.m[x][y][z]=w;
    };
}
