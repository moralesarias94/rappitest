/**
 * Created by Juan Daniel Morales on 10/22/2016.
 */
class Controller {
    static parseInput (input){
        var t=2;
        for(var i=0;i<t;i++){
            var n=3;
            var matrix = new Matrix3d(n,n,n);
            var m=3;
            for(var j=0;j<m;j++){
                var action = "QUERY";
                if(action==="QUERY"){
                    var x1=1;
                    var y1=1;
                    var z1=1;
                    var x2=1;
                    var y2=1;
                    var z2=1;
                    Query.sumBlocksQuery(x1,y1,z1,x2,y2,z2,matrix);
                }else if(action === "UPDATE"){
                    var x=2;
                    var y=2;
                    var z=2;
                    var w=4;
                    Update.updateBlock(x,y,z,w,matrix);
                }
            }

        }

    }
}
